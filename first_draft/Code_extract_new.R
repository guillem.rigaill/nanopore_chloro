#############################################################################################
## THIS FILE GENERATES DEPENDENCE ANALYSIS FOR 
## ALL BAM FILES IN dir_bam
## BAMS ARE ANALYSED ONE BY ONE 
## MERGED Ct bams and Pt bams ARE CREATED AND ANALYSED
##############################################
dir_bam <- "~/Documents/Data_Heavy/Data_Marine_Nano/"

#############################################################################################
## First load a number of packages
##############################################
library(GenomicRanges)
library("GenomicAlignments")
library(rtracklayer)
library(Rsamtools)
library(stringr)
library(Matrix)
library(parallel); mc.cores=4 ## NOT USED FOR NOW
###############################################
## source some utility functions
###############################################
source("Code_extract_utils.R")
source("Code_extract_process_function.R")
###############################################

###############################################
## recover editing sites, introns etc...
###############################################
editing_sites <- rtracklayer::import("TAIR10_ChrC_editing_sites.gff3") ## TODO? change ChrC to Pt in these annotation files
intron_sites  <- rtracklayer::import("TAIR10_ChrC_introns.gff3")       ## TODO? change ChrC to Pt in these annotation files

## change chr name
seqlevels(editing_sites); seqlevels(intron_sites); ## name of Chromosome is not Pt
seqlevels(editing_sites) <- seqlevels(intron_sites) <- c(ChrC="Pt")


## give a unique identifier for all sites (edition and introns)
mcols(editing_sites)$id <- paste0("edition_",1:length(editing_sites)); 
mcols(intron_sites)$id  <- paste0("intron_", 1:length(intron_sites )); 

all_sites <- c(editing_sites, intron_sites)

##############################################################################################
## PRE-PROCESSIN BAM FILES...
###############################################
## get files to modify depending on the location of the bams
bam.files <- list.files(dir_bam, full.names=T, pattern=".bam$")
prefix.out <- gsub(".*//|\\.bam", "", bam.files) ## not.used for now check regexp


###############################################
## READ FROM BAM AND CREATE INDEX IF NOT THERE
for(i_file in 1:length(bam.files)){
bam_file <- bam.files[i_file]
if(!file.exists(paste0(bam_file, ".bai"))){ ## make bai if not found
   indexBam(bam_file)
}
}

###############################################
## MERGE ALL Ct AND Pt BAMS IN ONE (if not already there)
###############################################
library(Rsamtools)
ct_replicates <- bam.files[grep("Ct.", bam.files)]
ctBam.files <- "~/Documents/Data_Heavy/Data_Marine_Nano/CT_All_rep.bam"
if(!file.exists(ctBam.files)) {
  mergeBam(ct_replicates, ctBam.files)
  indexBam(ctBam.files)
}

pt_replicates <- bam.files[grep("Pt.", bam.files)]
ptBam.files <- "~/Documents/Data_Heavy/Data_Marine_Nano/PT_All_rep.bam"
if(!file.exists(ptBam.files)) { 
 mergeBam(pt_replicates, ptBam.files)
 indexBam(ptBam.files)
}

###############################################
## RECOVER ALL BAM FILES (again)
###############################################
## get files to modify depending on the location of the bams
bam.files <- list.files(dir_bam, full.names=T, pattern=".bam$")
prefix.out <- gsub(".*//|\\.bam", "", bam.files) ## not.used for now check regexp


##############################################################################################
## LOOP OVER ALL FILES (ONE REPLICATE AT A TIME)
###############################################
for(i_file in 1:8){ ## 3 Ct, 3 Pt + 1 Ct ALL + 1 Pt ALL
print(i_file)
bam_file <- bam.files[i_file]

## Restrict to the Pt IRange
all_Pt_range <- IRangesList(Pt=IRanges(1, 200000)) ## TO CHANGE/ SELECT ONLY CHROMOSOME PT WITHOUT RANGE
scan_param <- ScanBamParam(which=all_Pt_range, what=c("seq","qname","cigar","pos", "strand"))

## READ BAM
all_reads <- as(GenomicAlignments::readGAlignments(file=bam_file, 
  				param=scan_param), "GRanges")


## COUNT AND ANALYSE DEPENDENCE (FISHER EXACT TEST)
output.mat <- getContingencyInfo(all_reads, all_sites)

## OUTPUT IN A CSV FILE
write.table(output.mat, file=paste0("output_analysis/", prefix.out[i_file], "_Fisher_simple_new.csv"), col.names=T, row.names=F)
}



