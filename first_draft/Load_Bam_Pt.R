## TEST SCRIPT TO LOAD THE BAM FILES
##############################################
library(GenomicRanges)
library("GenomicAlignments")
library(rtracklayer)
library(Rsamtools)
library(stringr)
library(Matrix)
library(parallel); mc.cores=4

## get file
bam.files <- list.files("~/Documents/Data_Heavy/Data_Marine_Nano/", full.names=T, pattern=".bam$")
prefix.out <- gsub(".*//|.bam", "", bam.files)
i_file <- 3


bam_file <- bam.files[i_file]
if(!file.exists(paste0(bam_file, ".bai"))){ ## make bai if not found
   indexBam(bam_file)
}

## Recover only Plastid reads
all_Pt_range <- IRangesList(Pt=IRanges(1, 200000))
scan_param <- ScanBamParam(which=all_Pt_range, what=c("seq","qname","cigar","pos"))
all_reads <- as(GenomicAlignments::readGAlignments(file=bam_file, 
  				param=scan_param), "GRanges")
  				
  				
length(all_reads)
length(unique(all_reads$qname))
hist(log2(all_reads@ranges@width))
sum(all_reads@ranges@width > 2^13)
  				


