## FUNCTION TO ANNOTATE ALL READS
## RETURN 
## (1 ANNOTONLY=TRUE)  A MATRIX WITH ONE LINE PAR READ AND ONE COLUMN PER EVENT (TODO REPLACE WITH SPARSEMATRIX FORMAT)
## (2 ANNOTONLY=FALSE) A DATA FRAME REPORTING CONTINGENCY ANALYSIS FOR ALL EVENT PAIRS
## FILTERZZ TO DISCARD PAIRS WITH 0 COUNTS

getContingencyInfo <- function(all_reads, all_sites, annotOnly=FALSE, filterZZ=TRUE){

###############################################
## SOME SIMPLE CHECKS PRIOR TO FILTERING
###############################################
length(all_reads)
length(unique(all_reads$qname))
sum(all_reads@ranges@width > 2^13)
###############################################
## ONLY CONSIDER READ OVERLAPPING SITES
###############################################
all_reads <- subsetByOverlaps(all_reads, all_sites)
## for CT1 quite a few duplicate and many read larger than 2^13?

# read rds directly
# all_reads_ <- readRDS("Pt_alg.rds")
## keep only reads mapping editing sites
# all_reads <- subsetByOverlaps(all_reads_, all_sites)

###############################################
## some filtering on qnames and length
## for now whole are removed (fast-track)
###############################################
## remove duplicates
unique_qnames <- names(which(table(all_reads$qname) == 1))
all_reads <- all_reads[all_reads$qname %in% unique_qnames]

## length (for CT1 after unique no more 2^13)
sum(all_reads@ranges@width > 2^13)
long_reads <- all_reads[all_reads@ranges@width >= 2^13] 
all_reads  <- all_reads[all_reads@ranges@width < 2^13]

##############################################
## read cigar to get genomic positions pre-calculation
##############################################
read_cigar <- data.frame(cigar = all_reads$cigar, start = start(all_reads))    
system.time(genomePosition_reads <- lapply(
  1:nrow(read_cigar), 
  function(i) getBasePosition_test(read_cigar[i,1], read_cigar[i, 2])
))

###############################################
## get pair editing - read 
###############################################
map_read_to_site <- GenomicAlignments::findOverlaps(all_reads, all_sites)

read_to_all_sites <- split(map_read_to_site@to, f=map_read_to_site@from)
## view of co-events
table(sapply(read_to_all_sites, length))


# initialize list with read annotations
annotated_reads.names  <- unique(all_reads$qname) ## TODO check not useful as duplicate have been removed
annotated_reads        <- vector("list", length(annotated_reads.names))
names(annotated_reads) <- annotated_reads.names; rm(annotated_reads.names)



###############################################
## loop over all pairs
###############################################
## first extract sequence and site info in data.frame for speed
read_seq  <- data.frame(seq=as.character(all_reads$seq))
site_info <- data.frame(start=start(all_sites), end=end(all_sites), id=all_sites$id, type=gsub("_.*", "", all_sites$id))
for(i in seq_along(map_read_to_site)){ 
  i_read <- map_read_to_site@from[i]
  i_site <- map_read_to_site@to[i] 
  seq <- read_seq[i_read, 1]  # query (read)
  if(site_info$type[i_site] == "edition"){
    annotated_reads[[i_read]][[site_info$id[i_site]]] <- info.edited(seq, site_info$start[i_site], genomePosition_reads[[i_read]]) #return NA or 1 or 0
  }
  if(site_info$type[i_site] == "intron"){
    annotated_reads[[i_read]][[site_info$id[i_site]]] <- info.spliced( site_info[i_site, ], genomePosition_reads[[i_read]])
  }
};




all_reads_strand <- as.character(strand(all_reads))
annotated_reads_simple <- lapply(
  seq_along(annotated_reads), 
  FUN=function(i){
    strand <- all_reads_strand[[i]]
    lapply(
      annotated_reads[[i]], 
      FUN=function(site){
        convert.info(site, strand)
      }
    )
  }
)

names(annotated_reads_simple) <- names(annotated_reads)
####################################################
## Formating
####################################################
## recover a matrix (replace by sparseMatrix if more events)
annotated_reads_vec <- lapply(seq_along(annotated_reads_simple), FUN=function(i){
  vec <- character(nrow(site_info))
  vec[match(names(annotated_reads[[i]]), site_info$id)] <- unlist(annotated_reads_simple[[i]])
  vec
})
matrixWithAllEvents <- do.call(rbind, annotated_reads_vec)
colnames(matrixWithAllEvents) <- site_info$id


if(annotOnly) return(matrixWithAllEvents)

####################################################
## loop over all pairs and test Fisher
####################################################
n_evt <- ncol(matrixWithAllEvents)
simple_levels <- c("True", "False", "Err")


listPair <- combn(length(all_sites), 2, simplify=F)
output <- lapply(listPair, FUN=function(x){
   i <- x[1]; j <- x[2];
   Xi <- factor(matrixWithAllEvents[, i], levels=simple_levels )
   Xj <- factor(matrixWithAllEvents[, j], levels=simple_levels )
   ctg_table <- table(Xj, Xi)
   status=sum(ctg_table[1:2, 1:2]) > 0
   pval=fisher.test(ctg_table[1:2, 1:2])$p.value
   return(data.frame(evt1=i, evt2=j, status=status, p.value=pval, 
     TT=ctg_table[1, 1], TF=ctg_table[2, 1], 
     FT=ctg_table[1, 2], FF=ctg_table[2, 2], 
     TErr=ctg_table[3, 1], FErr=ctg_table[3, 2],
     ErrT=ctg_table[1, 3], ErrF=ctg_table[2, 3], ErrErr=ctg_table[3, 3]))
})

####################################################
## some formating
####################################################
output.mat <- do.call(rbind, output)
output.mat$pos1 <- start(all_sites)[output.mat$evt1]
output.mat$strand1 <- as.vector(strand(all_sites))[output.mat$evt1]
output.mat$type1 <- colnames(matrixWithAllEvents)[output.mat$evt1]
output.mat$pos2 <- start(all_sites)[output.mat$evt2]
output.mat$strand2 <- as.vector(strand(all_sites))[output.mat$evt2]
output.mat$type2 <- colnames(matrixWithAllEvents)[output.mat$evt2]

if(filterZZ){
output.mat <- output.mat[output.mat$status, ]
}
## FDR control 
output.mat$adj.p.value <- p.adjust(output.mat$p.value, method="fdr")
output.mat$pos1 <- start(all_sites)[output.mat$evt1]
output.mat$strand1 <- as.vector(strand(all_sites)[output.mat$evt1])
output.mat$pos2 <- start(all_sites)[output.mat$evt2]
output.mat$strand2 <- as.vector(strand(all_sites)[output.mat$evt2])

output.mat <- output.mat[order(output.mat$p.value), ]
nTot <- (output.mat$TT+output.mat$TF+output.mat$FT+output.mat$FF)
pT1  <- (output.mat$TT+output.mat$TF)/nTot
pT2  <- (output.mat$TT+output.mat$FT)/nTot
output.mat$ExpTT <- signif(pT1*pT2*nTot, 3)
output.mat$ExpTF <- signif(pT1*(1-pT2)*nTot, 3)
output.mat$ExpFT <- signif((1-pT1)*pT2*nTot, 3)
output.mat$ExpFF <- signif((1-pT1)*(1-pT2)*nTot, 3)
output.mat$pT1   <- pT1
output.mat$pT2   <- pT2


return(output.mat)
}
##
