# -*- coding: utf-8 -*-



import csv

import matplotlib.pyplot as plt

import numpy

import math





#The data gets resampled over a table of following resolution:

resolution = 1500



#Display the data up to this x value (x is the first column)

range_x = 3000



#This value correspond to full black

range_y = 100.0





#Each value from in put data gets "splatted" on the image using a gaussian kernel

#Be careful, increasing these values too much may cause the calculation time to explode



#This sets the gaussian kernel base standard deviation for x= max size

sigma_x0 = 5.0



#This sets the enlargement of the gaussian kernel with decreasing x

#Set to zero for constant standard deviation over the whole set

sigma_x1 = 0.0



#Quality of calculation : up to n*sigma

nsigma = 4





#This is read from file

num_sets = 0;      

set_names = []



#Read and splat the data on the go

with open('./size_distrib.txt', 'r') as input:

    reader = csv.reader(input, delimiter='\t')

    header = True

    for t in reader:

        if(len(t)>0):

            if(header):

                num_sets = len(t)-1

                set_names = t[1:]

                header = False

                values = numpy.zeros([resolution,num_sets])

            else:

                x = float(t[0].replace(",","."))

                sigma_x = sigma_x0 + (range_x-x)*sigma_x1

                x1 = x-sigma_x*nsigma

                x2 = x+sigma_x*nsigma

                n1 = min(max(int(math.floor(x1/range_x*resolution)), 0), resolution-1)

                n2 = min(max(int(math.ceil(x2/range_x*resolution)), 0), resolution-1)

                i = 0

                erf_factor = 1.0/(math.sqrt(2.0)*sigma_x)

                

                for val in t[1:]:

                    y = float(val.replace(",", "."))                    

                    for n in range(n1, n2):

                        x1 = float(n)*range_x/resolution

                        x2 = float(n+1)*range_x/resolution

                        coef = math.erf((x2-x)*erf_factor)-math.erf((x1-x)*erf_factor)

                        values[n,i] = values[n,i] + coef*y

                    i = i+1

                

print(set_names)



plt.figure(figsize = (10,10), dpi=300)

fig, ax = plt.subplots(1,1,figsize=(10,10))

img = ax.imshow(values, cmap="gray_r", vmax=range_y, aspect=0.003) 

ax.set_yticks([i for i in range(0,resolution,100)])

ax.set_yticklabels([i*range_x/resolution for i in range(0,resolution,100)])

ax.set_xticks([0,1,2,3])

ax.set_xticklabels(set_names)


fig.colorbar(img)

plt.gca().invert_yaxis()


plt.savefig('vNB.png', dpi=300)

plt.show() 

