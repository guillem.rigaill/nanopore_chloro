# Using contrast to assess chloroplastic RNA co-maturation events

This is the Work In Progress conducted during an internship at IPS2 - OGE under the supervision of B.Castandet, G.Rigaill and E.Delannoy.

This sub-repository contains the R scripts for the search of *A.thaliana* chloroplastic RNA co-maturation events.

The code itself has been widely stolen from / inspired by the other scripts of this GitLab project.

## Contrast.R

This is the main R script and the only one that should be executed.
It contains the detailed pipeline from the retrieving of the bam files all the way to the exploration of the analysis results.

## data_formatting_utils

This is a script containing utility functions for the formatting of the nanopore RNA-seq data.

## data_exploration_utils

This is a script containing utility functions for statistical analysis and exploration of the nanopore RNA-seq data.

**Warning** The code is still unreviewed and insufficiently commented.
