#!/bin/bash

# concatenate all fastq files
find . -name "*[0_9].fastq.gz" -exec cat {} \; >raw_reads.fastq.gz

# Create the fasta file containing the 3' end adapter sequence
cat <<EOF > adapter.fa

>ONT-RT-3prime (partial)
GTACGTACAGAAGTGTTGCATTCA
EOF

# Create a substitution matrix for BC mapping
cat <<EOF > adapter.ma
#last -Q 0
#last -a 10
#last -A 10
#last -b 5
#last -B 5
#last -S 1
# score matrix (query letters = columns, reference letters = rows):
       A      C      G      T
A      4    -24     -9    -24
C    -24      5    -24    -14
G     -9    -24      7    -24
T    -24    -14    -24      8
EOF


# Construct the last index on the adapter
lastdb -uNEAR adapter.fa adapter.fa

# identify the 3'end adapter in the reads
lastal -Q0 -P10 -p adapter.ma adapter.fa <(zcat raw_reads.fastq.gz) | \
    maf-convert -n tab | \
    cut -f 2,7,10 | \
    uniq | \
    gzip > \
    adapter_assignments.txt.gz
	
	

# Reads with matching adapter indicating forward orientation
	zcat adapter_assignments.txt.gz | \
    awk '{if(($1 ~ /^ONT/ && $3 == "-")) {print $2}}' |\
    sort |
    uniq |\
    gzip >\
    fwd.txt.gz

# Reads with matching adapter indicating reverse orientation
zcat adapter_assignments.txt.gz | \
    awk '{if(($1 ~ /^ONT/ && $3 == "+")) {print $2}}' |\
    sort |
    uniq |\
    gzip >\
    rev.txt.gz

# keep reads that have only fwd information
comm -23 <(zcat fwd.txt.gz) <(zcat rev.txt.gz) |\
    gzip >\
    clean_fwd.txt.gz

# keep reads that have only rev information
comm -13 <(zcat fwd.txt.gz) <(zcat rev.txt.gz) |\
    gzip >\
    clean_rev.txt.gz

# keep reads that have both fwd and rev information
comm -12 <(zcat fwd.txt.gz) <(zcat rev.txt.gz) |\
    gzip >\
    amb.txt.gz


# All read id from the fastq
zcat raw_reads.fastq.gz |\
    sed -n '1p;1~4p' |\
    cut -f1 -d ' '|\
    gzip >\
   read_id.txt.gz


# Only the read id where no ONT adapter was found
zgrep -v -wF \
    -f <(zgrep -e "^ONT" adapter_assignments.txt.gz | cut -f2 | sort | uniq) \
    read_id.txt.gz |\
    gzip >\
    un.txt.gz
	
	
echo -n "Extracting the FWD reads... "
zgrep -wF -A 3 --no-group-separator --no-filename \
    -f <(zcat clean_fwd.txt.gz)\
    raw_reads.fastq.gz |\
    gzip >\
    fw_reads.fastq.gz
echo "Done."
echo -n "Extracting the REV reads... "
# forward on ONT primer
zgrep -wF -A 3 --no-group-separator --no-filename \
    -f <(zcat clean_rev.txt.gz)\
    raw_reads.fastq.gz | \
    gzip >\
    rev_reads.fastq.gz
echo "Done."
echo -n "Extracting undetermined reads... "
# Select the undetermined reads
zgrep -wF -A 3 --no-group-separator --no-filename \
    -f <(zcat un.txt.gz) \
    raw_reads.fastq.gz | \
    gzip >\
    un_reads.fastq.gz
echo "Done."
echo -n "Extracting ambiguous reads... "
# Select the ambiguous reads
zgrep -wF -A 3 --no-group-separator --no-filename \
    -f <(zcat amb.txt.gz) \
    raw_reads.fastq.gz | \
    gzip >\
    amb_reads.fastq.gz
echo "Done."
# Reverse complement rev reads
	zcat rev_reads.fastq.gz |\
            fastx_reverse_complement -z r>|\
            rev.RC.fastq.gz
# merge clean reads:
zcat fw_reads.fastq.gz rev.RC.fastq.gz > stranded_reads.fastq
rm fw_reads.fastq.gz
rm rev.RC.fastq.gz
rm rev_reads.fastq.gz
gzip stranded_reads.fastq
echo -n "Read stranding done."

## adapter trimming. First removing the 3' end adapter and the 4 random bases. then removing of the 5' adapter.
echo -n "Trimming adapters"
cutadapt -j 10 -e 0.25 -O 10 -a N{4}TGAATGCAACACTTCTGTACGTAC --untrimmed-output untrimmed3_reads.fastq.gz -o trimmed3.fastq.gz stranded_reads.fastq.gz
cutadapt -j 10 -e 0.25 -O 6 -g AAGCAGTGGTATCAACGCAGAGTACGGG --untrimmed-output untrimmed5_reads.fastq.gz -o trimmed.fastq.gz trimmed3.fastq.gz
rm trimmed3.fastq.gz
rm untrimmed3_reads.fastq.gz
rm untrimmed5_reads.fastq.gz
exit 0